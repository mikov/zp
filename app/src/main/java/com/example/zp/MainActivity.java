package com.example.zp;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    private final static String resultZp = "result";

    @BindView(R.id.total_zp) TextView totalZp;
    @BindView(R.id.day_textinputedittext) TextInputEditText amountOfDays;
    @BindView(R.id.evening_textinputedittext) TextInputEditText amountOfEvenings;
    @BindView(R.id.long_day_textinputedittext) TextInputEditText amountOfLongDays;
    @BindView(R.id.stavka_textinputedittext) EditText stavka;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(resultZp, totalZp.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        totalZp.setText(savedInstanceState.getString(resultZp));
    }

    @OnClick(R.id.show_zp_button)
    void showZp() {
        calcZp();
    }

    private void calcZp() {
        double eveningSurcharge = 1.2;
        double incomeTax = 0.87;
        double premium = 1.3;
        double result;
        double zpPerHour;
        int days;
        int evenings;
        int longDays;
        String formattedDouble;

        if (amountOfDays.getEditableText().toString().matches("")) {
            days = 0;
        } else {
            days = Integer.valueOf(amountOfDays.getEditableText().toString());
        }

        if (amountOfLongDays.getEditableText().toString().matches("")) {
            longDays = 0;
        } else {
            longDays = Integer.valueOf(amountOfLongDays.getEditableText().toString());
        }

        if (amountOfEvenings.getEditableText().toString().matches("")) {
            evenings = 0;
        } else {
            evenings = Integer.valueOf(amountOfEvenings.getEditableText().toString());
        }

        if (stavka.getEditableText().toString().matches("")) {
            Toast.makeText(getApplicationContext(), R.string.enter_stavka, Toast.LENGTH_LONG).show();
            totalZp.setText("");
        } else {
            zpPerHour = Double.valueOf(stavka.getEditableText().toString());
            result = (zpPerHour * days * 7.5 + zpPerHour * longDays * 8 + zpPerHour * evenings * 7.5 * eveningSurcharge) * premium * incomeTax;
            formattedDouble = new DecimalFormat("#0.00").format(result);
            totalZp.setText(formattedDouble);
        }
    }
}
